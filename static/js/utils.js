( function ( root ) {
    "use strict"
    /**
     * @namespace pharsas.utils
     */
    var pharsas = root.pharsas = root.pharsas || {},
        utils = pharsas.utils = pharsas.utils || {};
    /**
     * Store
     * @constructor
     */
    utils.Store = ( function() {
        var _ = utils._;
        /**
         * get from localstorage.
         * @param {String} key
         * @return {Object|Array|String} items
         */
        function _get( key ) {
            var i, k, items = [];
            if ( key === undefined ) {
                for( i = 0; i < localStorage.length; i++ ) {
                    k = localStorage.key( i );
                    items.push( JSON.parse( localStorage.getItem( k ) ) );
                }
                return items;
            }
            else {
                return JSON.parse( localStorage.getItem( key ) );
            }
        }
        /**
         * set to localstorage
         * @param {String} key
         * @param {Obj|Array|String} item
         */
         function _set( key, item ) {
            if ( !key || !item ) {
                return;
            }
            localStorage.setItem( key, JSON.stringify( item ) );
        }
        /**
         * update localstorage
         * @param {String} key
         * @param {Obj|Array|String} value must be same type of stored item.
         */
        function _update( key, value ) {
            var storedItem = _get( key ),
                i, key, keys;
            if ( !key || !value ) {
                return;
            }
            if ( !storedItem ) {
                _set( key, value );
            } else {
                if ( _.is( storedItem, "String" ) ) {
                    localStorage.setItem( key, JSON.stringify( value ) );
                } else if ( _.is( storedItem, "Array" ) ) {
                    if ( _.is( value, "Array" ) ) {
                        for ( i = 0; i < value.length; i++ ) {
                            storedItem.push( value[ i ] );
                        }
                        _set( key, storedItem );
                    }
                } else if ( _.is( storedItem, "Object" ) ) {
                    if ( _.is( value, "Object" ) ) {
                        keys = Object.keys( value );
                        for ( i = 0; i < keys.length; i++ ) {
                            key = keys[ i ];
                            storedItem[ key ] = value[ key ];
                        }
                        _set( key, storedItem );
                    }
                }
            }
        }
        /**
         * has item, value in array, object key?
         * @param {String} key
         * @param {String} target
         */
        function _has( key, target ) {
            var storedItem = _get( key );
            if ( !key || !target ) {
                return;
            }
            if ( !storedItem ) {
                return false;
            } else {
                if ( _.is( storedItem, "String" ) ) {
                    return _.is( target, "String" );
                } else if ( _.is( storedItem, "Array" ) ) {
                    return storedItem.indexOf( target ) !== -1 ? true : false;
                } else if (  _.is( storedItem, "Object" ) ) {
                    return storedItem.hasOwnProperty( target );
                } else {
                    return false;
                }
            }
        }
        /**
         * remove all items in localstorage
         */
        function _clear() {
            localStorage.clear();
        }
        /**
         * remove key items in localstorage
         */
        function _remove( key ) {
            window.localStorage.removeItem( key );
        }
        return {
            get: _get,
            set: _set,
            update: _update,
            has: _has,
            clear: _clear,
            remove: _remove
        };
    }());
    /**
     * Cache
     * @constructor
     */
    utils.Cache = ( function() {
        var _cache = {};
        /**
         * get from cache.
         * @param {String} key
         * @return {String|Object|Array} cache
         */
        function _get( key ) {
            if ( key === undefined ) {
                return _cache;
            }
            else {
                return _cache[key];
            }
        }
        /**
         * set to cache.
         * @param {Object} object
         */
        function _set( object ) {
            for ( key in object ) {
                _cache[key] = object[key];
            }
        }
        return {
            get: _get,
            set: _set
        };
    }());
    /**
     * PubSub
     * @constructor
     */
    utils.PubSub = {
        // subscribers: { any: [] },
        on: function(type, listener, context) {
            type = type || 'any';
            listener = (typeof listener === 'function') ?
                listener :
                (context ? context[listener] : undefined);

            if (!this.subscribers) {
                console.error('this.subscribers should be defined in constructor of each class.');
            }
            this.subscribers[type] = this.subscribers[type] || [];
            this.subscribers[type].push({
                listener: listener,
                context: context || this
            });
            return this;
        },

        off: function(type, listener, context) {
            return this.visitSubscribers('unsubscribe', type, listener, context);
        },

        trigger: function(type, publication) {
            return this.visitSubscribers('publish', type, publication);
        },

        visitSubscribers: function(action, type, arg, context) {
            var subscribers, i, max, listener;

            type = type || 'any';
            subscribers = this.subscribers[type] || [];

            for (i = 0, max = subscribers.length; i < max; i++) {
                listener = subscribers[i].listener;
                if (!listener) {
                    break;
                }
                if (action === 'publish') {
                    listener.apply(subscribers[i].context, arg);
                } else {
                    if (listener === arg && subscribers[i].context === context) {
                        subscribers.splice(i, 1);
                    }
                }
            }
            return this;
        }
    };
    /**
     * Class
     * @constructor
     */
    var Class;
    utils.Class = Class = function Class() {};

    Class.extend = function extend(props) {
        var SuperClass = this;

        function Class() {
            if (typeof this.init === 'function') {
                this.init.apply(this, arguments);
            }
        }
        Class.prototype = Object.create(SuperClass.prototype, {
            constructor: {
                value: Class,
                writable: true,
                configurable: true
            }
        });
        Object.keys(props).forEach(function(key) {
            Class.prototype[key] = props[key];
        });
        Object.keys(props).forEach(function(key) {
            var prop = props[key],
                _super = SuperClass.prototype[key],
                isMethodOverride = typeof prop === 'function' && typeof _super === 'function',
                isClass = isMethodOverride ? _super.name === 'Class' : false;

            Class.prototype[key] = isMethodOverride && !isClass ?
                (function(method, superMethod) {
                    return function() {
                        this._super = superMethod;
                        return method.apply(this, arguments);
                    };
                })(prop, _super) :
                prop;
        });
        Class.extend = SuperClass.extend;
        return Class;
    };
    /**
     * Underscore
     * @constructor
     */
    utils._ = ( function() {
        function _is( target , type ) {
            if ( !target || !type ) {
                return;
            }
            switch ( type ) {
                case "Array":
                    return Object.prototype.toString.call( target ) === "[object Array]";
                case "Object":
                    return Object.prototype.toString.call( target ) === "[object Object]";
                case "Number":
                    return Object.prototype.toString.call( target ) === "[object Number]";
                case "Boolean":
                    return Object.prototype.toString.call( target ) === "[object Boolean]";
                case "Undefined":
                    return Object.prototype.toString.call( target ) === "[object Undefined]";
                case "Null":
                    return Object.prototype.toString.call( target ) === "[object Null]";
                default:
                    return;
            }
        }
        function _zeroPaddingpad( num, size ) {
            var s = num + "";
            while ( s.length < size ) s = "0" + s;
            return s;
        }
        function _getJSTDate( date, type ) {
            var d, month, day, hour, minute;
            d = new Date( date + "+00:00" );
            month = _zeroPaddingpad( d.getMonth() + 1, 2 );
            day = _zeroPaddingpad( d.getDate(), 2 );
            hour = _zeroPaddingpad( d.getHours(), 2 );
            minute = _zeroPaddingpad( d.getMinutes(), 2 );

            if ( type === "yy/mm/dd" ) {
                return d.getFullYear() + "/" + month + "/" + day;
            } else if ( type === "yy/mm" ) {
                return d.getFullYear() + "/" + month;
            } else {
                return d.getFullYear() + "/" + month + "/" + day +
                   " " + hour + ":" + minute;
            }
        }
        function _existsHtmlEl( id ) {
            id = id.replace( /#/, "" );
            return ( document.getElementById( id ) ) ? true : false;
        }
        // Usage: _.isLocal() ? 'http://local...' : 'https://api...'
        function _isLocal() {
            if ( document.domain === "127.0.0.1" || document.domain === "localhost" ) {
                return true;
            }
            return false;
        }
        function _host() {
            return location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
        }
        function _getMicode() {
            return utils.Store.get( "micode" );
        }
        function _setMicode( micode ) {
            // Todo micodeが10桁でなければcookieを削除.index.htmlに飛ばす
            var micode = _zeroPaddingpad( micode, 10 );
            utils.Store.set( "micode", String(micode) );
        }
        function _getPharmacyId() {
            return utils.Store.get( "pharmacy-id" );
        }
        function _setPharmacyId( pharmacyId ) {
            utils.Store.set( "pharmacy-id", String(pharmacyId) );
        }
        function _getVendor() {
            return utils.Store.get( "vendor" );
        }
        function _setVendor( vendor ) {
            utils.Store.set( "vendor", String(vendor) );
        }
        function _getPharmacyInfo() {
            return JSON.parse( utils.Store.get( "pharmacy-info" ) );
        }
        function _setPharmacyInfo( pharmacy ) {
            pharmacy = pharmacy.replace( /&#34;/g, '"' );
            pharmacy = pharmacy.replace( /\n/g, "" );
            pharmacy = JSON.parse( pharmacy );
            utils.Store.set( "pharmacy-info", JSON.stringify( pharmacy ) );
        }
        function _getApp() {
            var pathname = window.location.pathname,
                appPathname = pathname.split( "/" )[ 1 ];

            switch( appPathname ) {
                case "":
                case "rxbox":
                    return "RXBOX";
                 case "account":
                    return "ACCOUNT";
                case "pharmacydata":
                    return "PHARMACYDATA"
                case "medshare":
                    return "MEDSHARE";
                case "rxanalytics":
                    return "RXANALYTICS"
                case "pickmonitor":
                    return "PICKMONITOR";
                default:
                    return null;
            };
        }
        return {
            is: _is,
            zeroPaddingpad: _zeroPaddingpad,
            getJSTDate: _getJSTDate,
            existsHtmlEl: _existsHtmlEl,
            isLocal: _isLocal,
            host: _host,
            getMicode: _getMicode,
            setMicode: _setMicode,
            getPharmacyId: _getPharmacyId,
            setPharmacyId: _setPharmacyId,
            getVendor: _getVendor,
            setVendor: _setVendor,
            getPharmacyInfo: _getPharmacyInfo,
            setPharmacyInfo: _setPharmacyInfo,
            getApp: _getApp
        };
    }());
})( window );


( function ( root ) {
    "use strict"

    var $ = root.$;

    // Cache Elements
    var cacheElements = {
        $contact: "#js-common-contact",
        $appTitle: "#js-common-app-title",
        $appMiniTitle: "#js-common-app-mini-title",
        $appMdTitle: "#js-common-app-md-title",
        $sidebarCollapse: "#js-common-sidebar-collapse",
    }
    // Contact in account
    $( cacheElements.$contact ).on( "click", function() {
        var to = "support@pharsas.com",
            subject = "【お客様サポート】トラブルお問い合わせ",
            body = "（トラブルを具体的にお知らせください。ご利用のアプリ名、OS（Windows、Mac）、ブラウザの種類（Internet Explorer、Google Chromeなど）も分かる範囲でお願いします。）";

        location.href = "mailto:" + to +
                        "?subject="+ subject +
                        "&body=" + body;
    });
    // Application title
    var appTitle, appMiniTitle;
    switch( pharsas.utils._.getApp() ) {
        case null:
        case "RXBOX":
            appTitle = "メニュー";
            appMiniTitle = "menu";
            break;
        case "ACCOUNT":
            appTitle = "アカウント";
            appMiniTitle = "Acc";
            break;
        case "PHARMACYDATA":
            appTitle = "薬局データ";
            appMiniTitle = "PDB";
            break;
        case "MEDSHARE":
            appTitle = "Med Share";
            appMiniTitle = "MdS";
            break;
        case "RXANALYTICS":
            appTitle = "PICK LINK";
            appMiniTitle = "PcL";
            break;
         case "PICKMONITOR":
            appTitle = "PICK MONITOR";
            appMiniTitle = "PcM";
            break;
    }
    if ( appTitle && appMiniTitle ) {
        $( cacheElements.$appTitle ).html( appTitle );
        $( cacheElements.$appMiniTitle ).html( appMiniTitle );
        $( cacheElements.$appMdTitle ).html( appTitle );
    }
})( window );
