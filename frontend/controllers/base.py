#!/usr/bin/env python
# -*- coding: utf-8 -*-
import jinja2
import webapp2


JINJA_ENV = jinja2.Environment(
    loader=jinja2.FileSystemLoader('frontend/views'),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)


class BaseHandler(webapp2.RequestHandler):
    def jinja2(self):
        return jinja2.get_jinja2(app=self.app)
    def render_template(self, filename, template_values, **template_args):
        template = JINJA_ENV.get_template(filename)
        self.response.out.write(template.render(template_values))

