#!/usr/bin/env python
# -*- coding: utf-8 -*-
from base import BaseHandler


class IndexHandler(BaseHandler):
    def get(self):
        self.render_template('main/index.html', {})

