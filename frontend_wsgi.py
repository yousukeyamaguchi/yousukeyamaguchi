#!/usr/bin/env python
# -*- coding: utf-8 -*-
import webapp2
from webapp2 import Route
import frontend.controllers.main as main

ROOT_PATH = ''

routes = [
	Route(ROOT_PATH + '/', main.IndexHandler),
]

app = webapp2.WSGIApplication(routes, debug=True)

